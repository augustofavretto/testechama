package com.example.testchama.model

data class GooglePlacesModel(val icon: String, val name: String, val address: String)