package com.example.testchama

import android.app.Application
import com.example.testchama.config.remoteModule
import com.example.testchama.config.repositoryModule
import com.example.testchama.config.uiModule
import com.facebook.drawee.backends.pipeline.Fresco
import org.koin.android.ext.android.startKoin

open class App : Application() {

    private val appModules by lazy {
        listOf(remoteModule, repositoryModule, uiModule)
    }

    override fun onCreate() {
        super.onCreate()

        startKoin(this, appModules)

        Fresco.initialize(this)
    }

}