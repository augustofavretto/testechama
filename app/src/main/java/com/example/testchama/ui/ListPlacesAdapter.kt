package com.example.testchama.ui
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.testchama.R
import com.example.testchama.databinding.ListPlaceItemBinding
import com.example.testchama.model.GooglePlacesModel
import com.facebook.drawee.generic.RoundingParams

class ListUsersAdapter(
    private val places: List<GooglePlacesModel>
) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return ListPlacesViewHolder(
            LayoutInflater
                .from(parent.context)
                .inflate(R.layout.list_place_item, parent, false)
        )
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val binding = (holder as ListPlacesViewHolder).binding
        val place = places[position]
        loadImage(binding, place)
        binding.place = place
    }

    private fun loadImage(binding: ListPlaceItemBinding, google: GooglePlacesModel) {
        binding.image.setImageURI(google.icon)
    }

    override fun getItemCount() = places.size

}

class ListPlacesViewHolder(view: View) : RecyclerView.ViewHolder(view) {

    val binding: ListPlaceItemBinding = ListPlaceItemBinding.bind(view)

}