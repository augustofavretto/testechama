package com.example.testchama.ui

import android.Manifest
import android.content.Context
import android.content.pm.PackageManager
import androidx.core.app.ActivityCompat
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.testchama.model.GooglePlacesModel
import com.example.testchama.resources.repository.GoogleRepository
import com.google.android.gms.location.LocationServices
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.SupervisorJob
import kotlinx.coroutines.launch
import java.lang.Exception
import kotlin.coroutines.resume
import kotlin.coroutines.resumeWithException
import kotlin.coroutines.suspendCoroutine

class MainViewModel(
    private val context: Context,
    private val repository: GoogleRepository
) : ViewModel() {
    private val viewModelJob = SupervisorJob()
    private val viewModeScope = CoroutineScope(Dispatchers.Main + viewModelJob)

    var listPlaces: MutableLiveData<List<GooglePlacesModel>> = MutableLiveData()
    var loading: MutableLiveData<Boolean> = MutableLiveData()
    var error: MutableLiveData<Throwable> = MutableLiveData()

    fun getAll() {
        viewModeScope.launch {
            if (validLocation()) {
                loading.value = true
                try {
                    getLastLocation(context)?.let {
                        val places = repository.getAll(it)
                        listPlaces.value = places
                    }
                } catch (t: Throwable) {
                    listPlaces.value = emptyList()
                    error.value = t
                } finally {
                    loading.value = false
                }
            }
        }
    }

    fun getByPlace(query: String) {
        viewModeScope.launch {
            if (validLocation()) {
                loading.value = true
                try {
                    getLastLocation(context)?.let {
                        val places = repository.getByPlace(query,it)
                        listPlaces.value = places
                    }
                } catch (t: Throwable) {
                    listPlaces.value = emptyList()
                    error.value = t
                } finally {
                    loading.value = false
                }
            }
        }
    }

    private fun validLocation(): Boolean {
        return if (isPermissionGiven()) {
            true
        } else {
            error.value = SecurityException("Location permission required")
            false
        }
    }

    private fun isPermissionGiven(): Boolean {
        return ActivityCompat.checkSelfPermission(
            context,
            Manifest.permission.ACCESS_FINE_LOCATION
        ) == PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(
            context,
            Manifest.permission.ACCESS_COARSE_LOCATION
        ) == PackageManager.PERMISSION_GRANTED
    }

    private suspend fun getLastLocation(context: Context): android.location.Location? =
        suspendCoroutine {
            if (isPermissionGiven()) {
                LocationServices.getFusedLocationProviderClient(context).lastLocation
                    .addOnSuccessListener { location ->
                        it.resume(location)
                    }
                    .addOnFailureListener { e ->
                        it.resumeWithException(e)
                    }
            } else
                it.resume(null)
        }

}