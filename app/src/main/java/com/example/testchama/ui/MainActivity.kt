package com.example.testchama.ui

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.Menu
import androidx.core.app.ActivityCompat
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.testchama.R
import com.example.testchama.databinding.ActivityMainBinding
import com.example.testchama.databinding.ActivityMainBindingImpl
import com.example.testchama.utils.PermissionUtils
import com.miguelcatalan.materialsearchview.MaterialSearchView
import org.koin.androidx.viewmodel.ext.android.viewModel

class MainActivity : AppCompatActivity() {

    private val viewModel by viewModel<MainViewModel>()

    private val binding by lazy {
        DataBindingUtil.setContentView<ActivityMainBinding>(this, R.layout.activity_main)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding.viewModel = viewModel
        binding.lifecycleOwner = this

        observeUsers()

        setupSearchView()
        setupRecyclerView()

    }

    override fun onResume() {
        super.onResume()
        askPermission()
    }

    private fun askPermission() {
        if (!PermissionUtils.isLocationGranted(this)) {
            ActivityCompat.requestPermissions(
                this,
                PermissionUtils.LOCATION,
                PERMISSION_ASK_LOCATION
            )
        }else{
            viewModel.getAll()
        }
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String>,
        grantResults: IntArray
    ) {
        when (requestCode) {
            PERMISSION_ASK_LOCATION -> {
                if (PermissionUtils.grantedResults(grantResults)) {
                    viewModel.getAll()
                }
                return
            }
        }
    }

    private fun observeUsers() {
        viewModel.listPlaces.observe(this, { places -> binding.recyclerView.adapter =
                ListUsersAdapter(places)
            }
        )

        viewModel.error.observe(this, {
            if (it is SecurityException){
                askPermission()
            }
        })
    }

    private fun setupSearchView() {
        setSupportActionBar(binding.toolbar)
        binding.searchView.setOnSearchViewListener(object : MaterialSearchView.SearchViewListener {
            override fun onSearchViewShown() { }
            override fun onSearchViewClosed() {
                viewModel.getAll()
            }
        })

        binding.searchView.setOnQueryTextListener(object : MaterialSearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String): Boolean {
                viewModel.getByPlace(query)
                return true
            }

            override fun onQueryTextChange(newText: String) = true
        })
    }

    private fun setupRecyclerView() {
        val dividerItemDecoration = DividerItemDecoration(
            binding.recyclerView.context,
            (binding.recyclerView.layoutManager as LinearLayoutManager).orientation
        )
        binding.recyclerView.addItemDecoration(dividerItemDecoration)
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.search_menu, menu)
        val item = menu.findItem(R.id.action_search)
        binding.searchView.setMenuItem(item)
        return true
    }

    companion object {
        const val PERMISSION_ASK_LOCATION = 500
    }
}