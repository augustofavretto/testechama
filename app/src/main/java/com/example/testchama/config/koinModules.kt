package com.example.testchama.config

import com.example.testchama.resources.api.GoogleApi
import com.example.testchama.resources.repository.GoogleDataRepository
import com.example.testchama.resources.repository.GoogleRepository
import com.example.testchama.ui.MainViewModel
import org.koin.android.ext.koin.androidContext
import org.koin.androidx.viewmodel.ext.koin.viewModel
import org.koin.dsl.module.module

val uiModule = module {
    viewModel { MainViewModel( androidContext() , get()) }
}

val repositoryModule = module {
    single<GoogleRepository> { GoogleDataRepository(androidContext(), get()) }
}

val remoteModule = module {
    single { provideOkHttpClient() }
    single { provideRetrofit(get()) }
    single { createApi<GoogleApi>(get()) }
}
