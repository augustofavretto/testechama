package com.example.testchama.resources.response

import com.example.testchama.model.GooglePlacesModel

data class ReponseGoogle( var results: List<GoogleResponse>)


fun ReponseGoogle.toModel(model: GoogleResponse) = GooglePlacesModel(model.avatarUrl, model.name, model.address)