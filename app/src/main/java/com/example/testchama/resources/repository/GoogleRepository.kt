package com.example.testchama.resources.repository

import android.location.Location
import com.example.testchama.model.GooglePlacesModel

interface GoogleRepository {

    suspend fun getAll(location : Location): List<GooglePlacesModel>

    suspend fun getByPlace(place: String, location: Location): List<GooglePlacesModel>

}