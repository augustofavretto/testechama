package com.example.testchama.resources.repository

import android.content.Context
import android.location.Location
import com.example.testchama.R
import com.example.testchama.resources.api.GoogleApi
import com.example.testchama.resources.response.toModel
import kotlinx.coroutines.Dispatchers.IO
import kotlinx.coroutines.withContext

class GoogleDataRepository(
    private val context: Context,
    private val api: GoogleApi
) : GoogleRepository {

    override suspend fun getAll(location: Location) = withContext(IO) {
        api.getAll(
            "${location.latitude},${location.longitude}",
            context.getString(R.string.api_key)
        ).await().results.map { it.toModel() }
    }

    override suspend fun getByPlace(place: String, location: Location) = withContext(IO) {
        api.getByPlace(
            "${location.latitude},${location.longitude}",
            place,
            context.getString(R.string.api_key)
        ).await().results.map { it.toModel() }
    }

}