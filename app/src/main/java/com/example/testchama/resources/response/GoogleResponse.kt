package com.example.testchama.resources.response

import com.example.testchama.model.GooglePlacesModel
import com.squareup.moshi.Json

data class GoogleResponse(
    @Json(name = "name") val name: String,
    @Json(name = "icon") val avatarUrl: String,
    @Json(name = "formatted_address") val address: String,
)

fun GoogleResponse.toModel() = GooglePlacesModel(this.avatarUrl, this.name, this.address)