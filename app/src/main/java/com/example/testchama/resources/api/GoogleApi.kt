package com.example.testchama.resources.api

import com.example.testchama.resources.response.ReponseGoogle
import kotlinx.coroutines.Deferred
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface GoogleApi {

    @GET("/maps/api/place/textsearch/json?query=restaurants+Cafes+Bars")
    fun getAll(
        @Query("location") location: String,
        @Query("key") appKey: String,
    ): Deferred<ReponseGoogle>

    @GET("/maps/api/place/textsearch/json?")
    fun getByPlace(
        @Query("location") location: String,
        @Query("input") input: String,
        @Query("key") appKey: String,
    ): Deferred<ReponseGoogle>

}